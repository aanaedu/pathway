const fs = require('fs');
const readline = require('readline');

const DATA_FILE_PATH = __dirname + "/input-large.in";
const OUTPUT_FILE_NAME_EXT = "output.dat";
const algoHelper = require('./algo-helper');

const DEBUG = true;

logMessage(`Reading File at: ${DATA_FILE_PATH}`);

function logMessage(message) {
    if (DEBUG) {
        console.log(message);
    }
}

function init() {
    const readStream = fs.createReadStream(DATA_FILE_PATH);
    const readLineInterface = readline.createInterface({
      input: readStream
    });

    let lineNumber = 1, sizeN = 0, sizeM = 0, caseCounter = 0, TEST_SIZE;
    let result = -1, output = '', pathList = [], lastNewDataLineNumber;
    let positionData = {
        sourceX: 0,
        sourceY: 0,
        destinationX: 0,
        destinationY: 0
    };
    let isTwoColsRows = false;

    readLineInterface.on('line', function (line) {
        if (lineNumber === 1) {
            TEST_SIZE = parseInt(line, 10);
        } else {
            const lineData = line.split(/\s+/).map(i => parseInt(i, 10));
            if (lineData.length === 2 && !isTwoColsRows) {
                // read next grid row and col size
                pathList = [];
                sizeN = lineData[0];
                sizeM = lineData[1];
                lastNewDataLineNumber = lineNumber;
                isTwoColsRows = (lineData[0] === 2 || lineData[1] === 2);
            } else if (lineNumber === lastNewDataLineNumber + 1) {
                // read source and destination points
                positionData.sourceX = lineData[0];
                positionData.sourceY = lineData[1];
                positionData.destinationX = lineData[2];
                positionData.destinationY = lineData[3];
            } else {
                // load grid items for next case
                pathList.push(lineData);

                // if completely loaded next case grid items
                if ((lineNumber - lastNewDataLineNumber) === sizeN + 1) {
                    isTwoColsRows = false;
                    caseCounter++;
                    result = algoHelper.cost(pathList, sizeN, sizeM, positionData);
                    if (result === -1) {
                        output += `Case #${caseCounter}: Mission Impossible.\n`;
                    } else {
                        output += `Case #${caseCounter}: ${result}\n`;
                    }
                }
            }
        }

        // stop reading file
        if (caseCounter === TEST_SIZE) {
            readLineInterface.close();
            readStream.destroy();
        }
        lineNumber++;
    });


    readLineInterface.on('close', () => {
        // build output
        logMessage("Writing output to file...");
        fs.writeFileSync(OUTPUT_FILE_NAME_EXT, output);
        logMessage(`Done writing output to file "${ OUTPUT_FILE_NAME_EXT }". Exiting process...`);
        process.exit(0);
    });
}

// start program
init();
