// defining point
const Point = (function () {
    function Point(x, y) {
        this.row = x;
        this.col = y;
    }
    return Point;
}());

// defining node
const GraphNode = (function () {
    function GraphNode(point, distance, weight) {
        this.point = point;
        this.distance = distance;
        this.weight = weight;
    }
    return GraphNode;
}());

function getPointWeight(grid, point) {
    return grid[ point.row ][ point.col ];
}

function getPointKey(point) {
    return `${point.row}_${point.col}`;
}

function isGridObstacle(weight) {
    return weight === -1;
}

function isPointVisited(point, visitedDict) {
    return visitedDict.hasOwnProperty(getPointKey(point));
}

function isValidPoint(point, gridRowSize, gridColSize) {
    return (point.row >=0 && point.row < gridRowSize) && (point.col >=0 && point.col < gridColSize);
}


exports.cost = function(grid, gridRowSize, gridColSize, position) {
    // initialize
    const sourcePoint = new Point(position.sourceX, position.sourceY);
    const sourceWeight = getPointWeight(grid, sourcePoint);
    const sourceNode = new GraphNode(sourcePoint, 0, sourceWeight);

    // base
    if (isGridObstacle(sourceWeight)) {
        return -1;
    }

    const destinationPoint = new Point(position.destinationX, position.destinationY);
    const basicQueue = [];
    let currentPoint, currentNode, visitedDict = {};

    // ENQUEUE source node
    basicQueue.push(sourceNode);
    visitedDict[getPointKey(sourcePoint)] = true;

    while (basicQueue.length !== 0) {
        // DEQUEUE
        currentNode = basicQueue.shift();
        currentPoint = currentNode.point;

        // if destination node is reached
        if (destinationPoint.row === currentPoint.row && destinationPoint.col === currentPoint.col) {
            return currentNode.weight;
        }

        const rowValues = [ -1, 0, 0, 1 ];
        const colValues = [ 0, -1, 1, 0 ];

        currentPoint.row = parseInt(currentPoint.row, 10);
        currentPoint.col = parseInt(currentPoint.col, 10);

        for(let i = 0; i < rowValues.length; i++) {
            let edgeRow = currentPoint.row + rowValues[i];
            let edgeCol = currentPoint.col + colValues[i];
            const edgePoint = new Point(edgeRow, edgeCol);
            if (isValidPoint(edgePoint, gridRowSize, gridColSize) && !isPointVisited(edgePoint, visitedDict)) {
                const edgeWeight = getPointWeight(grid, edgePoint);
                if (!isGridObstacle(edgeWeight)) {
                    visitedDict[getPointKey(edgePoint)] = true;
                    // ENQUEUE
                    basicQueue.push(new GraphNode(edgePoint, currentNode.distance + 1, currentNode.weight + edgeWeight));
                }
            }
        }
    }
    return -1;
};
