#!/bin/bash

# check if node is installed
if which node > /dev/null
then
echo "node is installed, continuing..."
node ./main.js
else
echo "node is not installed, skipping..."
echo "Please visit https://nodejs.org/en/download/ for installation instructions."
fi

